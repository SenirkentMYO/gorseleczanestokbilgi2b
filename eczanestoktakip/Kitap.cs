//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eczanestoktakip
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kitap
    {
        public int ID { get; set; }
        public string Adi { get; set; }
        public string BasimYili { get; set; }
        public Nullable<int> BasimYeriID { get; set; }
        public Nullable<int> YayinEviID { get; set; }
        public Nullable<int> KaydedenID { get; set; }
        public Nullable<System.DateTime> KayitTarihi { get; set; }
        public Nullable<int> DegistirenID { get; set; }
        public Nullable<System.DateTime> DegisiklikTarihi { get; set; }
    }
}
