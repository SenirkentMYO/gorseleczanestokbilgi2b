﻿namespace eczanestoktakip
{
    partial class İlacBilgileri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ilckytbtn = new System.Windows.Forms.Button();
            this.tmilcbtn = new System.Windows.Forms.Button();
            this.brkdtxt = new System.Windows.Forms.TextBox();
            this.barkodno = new System.Windows.Forms.Label();
            this.ilcsilbtn = new System.Windows.Forms.Button();
            this.txtilacAd = new System.Windows.Forms.TextBox();
            this.txtBarkdNo = new System.Windows.Forms.TextBox();
            this.txtTuru = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtüretm = new System.Windows.Forms.TextBox();
            this.txtson = new System.Windows.Forms.TextBox();
            this.txtfiyat = new System.Windows.Forms.TextBox();
            this.txtürt = new System.Windows.Forms.TextBox();
            this.txtkutu = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(735, 283);
            this.dataGridView1.TabIndex = 0;
            // 
            // ilckytbtn
            // 
            this.ilckytbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ilckytbtn.Location = new System.Drawing.Point(605, 598);
            this.ilckytbtn.Name = "ilckytbtn";
            this.ilckytbtn.Size = new System.Drawing.Size(104, 40);
            this.ilckytbtn.TabIndex = 1;
            this.ilckytbtn.Text = "İLAC KAYDET";
            this.ilckytbtn.UseVisualStyleBackColor = true;
            this.ilckytbtn.Click += new System.EventHandler(this.ilckytbtn_Click);
            // 
            // tmilcbtn
            // 
            this.tmilcbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tmilcbtn.Location = new System.Drawing.Point(12, 374);
            this.tmilcbtn.Name = "tmilcbtn";
            this.tmilcbtn.Size = new System.Drawing.Size(104, 40);
            this.tmilcbtn.TabIndex = 2;
            this.tmilcbtn.Text = "TÜM İLACLAR";
            this.tmilcbtn.UseVisualStyleBackColor = true;
            this.tmilcbtn.Click += new System.EventHandler(this.tmilcbtn_Click);
            // 
            // brkdtxt
            // 
            this.brkdtxt.Location = new System.Drawing.Point(138, 394);
            this.brkdtxt.Name = "brkdtxt";
            this.brkdtxt.Size = new System.Drawing.Size(161, 20);
            this.brkdtxt.TabIndex = 3;
            // 
            // barkodno
            // 
            this.barkodno.AutoSize = true;
            this.barkodno.Location = new System.Drawing.Point(135, 374);
            this.barkodno.Name = "barkodno";
            this.barkodno.Size = new System.Drawing.Size(58, 13);
            this.barkodno.TabIndex = 4;
            this.barkodno.Text = "Barkod No";
            // 
            // ilcsilbtn
            // 
            this.ilcsilbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ilcsilbtn.Location = new System.Drawing.Point(325, 374);
            this.ilcsilbtn.Name = "ilcsilbtn";
            this.ilcsilbtn.Size = new System.Drawing.Size(103, 40);
            this.ilcsilbtn.TabIndex = 5;
            this.ilcsilbtn.Text = "İLAÇ SİL";
            this.ilcsilbtn.UseVisualStyleBackColor = true;
            // 
            // txtilacAd
            // 
            this.txtilacAd.Location = new System.Drawing.Point(553, 301);
            this.txtilacAd.Name = "txtilacAd";
            this.txtilacAd.Size = new System.Drawing.Size(100, 20);
            this.txtilacAd.TabIndex = 6;
            // 
            // txtBarkdNo
            // 
            this.txtBarkdNo.Location = new System.Drawing.Point(553, 344);
            this.txtBarkdNo.Name = "txtBarkdNo";
            this.txtBarkdNo.Size = new System.Drawing.Size(100, 20);
            this.txtBarkdNo.TabIndex = 7;
            // 
            // txtTuru
            // 
            this.txtTuru.Location = new System.Drawing.Point(553, 386);
            this.txtTuru.Name = "txtTuru";
            this.txtTuru.Size = new System.Drawing.Size(100, 20);
            this.txtTuru.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(492, 308);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "ilacAdi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(492, 351);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "BarkodNo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(492, 393);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Turu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(480, 430);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "ÜretimTarihi";
            // 
            // txtüretm
            // 
            this.txtüretm.Location = new System.Drawing.Point(549, 427);
            this.txtüretm.Name = "txtüretm";
            this.txtüretm.Size = new System.Drawing.Size(100, 20);
            this.txtüretm.TabIndex = 13;
            // 
            // txtson
            // 
            this.txtson.Location = new System.Drawing.Point(549, 468);
            this.txtson.Name = "txtson";
            this.txtson.Size = new System.Drawing.Size(100, 20);
            this.txtson.TabIndex = 14;
            // 
            // txtfiyat
            // 
            this.txtfiyat.Location = new System.Drawing.Point(549, 499);
            this.txtfiyat.Name = "txtfiyat";
            this.txtfiyat.Size = new System.Drawing.Size(100, 20);
            this.txtfiyat.TabIndex = 15;
            // 
            // txtürt
            // 
            this.txtürt.Location = new System.Drawing.Point(553, 536);
            this.txtürt.Name = "txtürt";
            this.txtürt.Size = new System.Drawing.Size(100, 20);
            this.txtürt.TabIndex = 16;
            // 
            // txtkutu
            // 
            this.txtkutu.Location = new System.Drawing.Point(549, 571);
            this.txtkutu.Name = "txtkutu";
            this.txtkutu.Size = new System.Drawing.Size(100, 20);
            this.txtkutu.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(483, 475);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Sonkullanim";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(492, 506);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Fiyat";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(479, 543);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "ÜretenFirma";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(479, 578);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "KutuSayisi";
            // 
            // İlacBilgileri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 650);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtkutu);
            this.Controls.Add(this.txtürt);
            this.Controls.Add(this.txtfiyat);
            this.Controls.Add(this.txtson);
            this.Controls.Add(this.txtüretm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTuru);
            this.Controls.Add(this.txtBarkdNo);
            this.Controls.Add(this.txtilacAd);
            this.Controls.Add(this.ilcsilbtn);
            this.Controls.Add(this.barkodno);
            this.Controls.Add(this.brkdtxt);
            this.Controls.Add(this.tmilcbtn);
            this.Controls.Add(this.ilckytbtn);
            this.Controls.Add(this.dataGridView1);
            this.Name = "İlacBilgileri";
            this.Text = "İlacBilgileri";
            this.Load += new System.EventHandler(this.İlacBilgileri_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button ilckytbtn;
        private System.Windows.Forms.Button tmilcbtn;
        private System.Windows.Forms.TextBox brkdtxt;
        private System.Windows.Forms.Label barkodno;
        private System.Windows.Forms.Button ilcsilbtn;
        private System.Windows.Forms.TextBox txtilacAd;
        private System.Windows.Forms.TextBox txtBarkdNo;
        private System.Windows.Forms.TextBox txtTuru;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtüretm;
        private System.Windows.Forms.TextBox txtson;
        private System.Windows.Forms.TextBox txtfiyat;
        private System.Windows.Forms.TextBox txtürt;
        private System.Windows.Forms.TextBox txtkutu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}