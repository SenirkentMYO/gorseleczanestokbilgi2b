﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace eczanestoktakip
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            KitapDBEntities database = new KitapDBEntities();
            List<VwKitap> kitapListesi = database.VwKitap.ToList();
            dataGridView1.DataSource = kitapListesi;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            KitapDBEntities database = new KitapDBEntities();
            List<VwKitap> ktp = database.VwKitap.ToList();
            dataGridView1.DataSource = ktp;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            KitapDBEntities database = new KitapDBEntities();
            List<VwKullanici> persnel = database.VwKullanici.ToList();
            dataGridView1.DataSource = persnel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
             KitapDBEntities db = new KitapDBEntities();
            List<Kullanici> kliste = db.Kullanici.Where(tablom => tablom.Email == textBox1.Text && tablom.Sifre == textBox2.Text).ToList();
            if (kliste.Count > 0)
            {
                İlacBilgileri f = new İlacBilgileri();
                MessageBox.Show("Giris Basarili");
                f.Show();
            }
            else
                MessageBox.Show("Hatalı giriş Yaptınız Tekrar Deneyin");
        }
        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void hstbtn_Click(object sender, EventArgs e)
        {
            EczaneTakipEntities db = new EczaneTakipEntities();
            List<HastaTb> hasta = db.HastaTb.ToList();
            dataGridView1.DataSource = hasta;

        }

      
        }
    }

